import time
import board
import neopixel
import random
import threading
import sys
import ast

# raspberry pi pin for neopixels
pixel_pin = board.D18

# The number of NeoPixels
num_pixels = 45

# Color order for neopixels
ORDER = (1,0,2)

pixels = neopixel.NeoPixel(pixel_pin, num_pixels, brightness=0.2, auto_write=False,
                           pixel_order=ORDER)

dimming = 0.4

primary_col = (255, 102, 0)
secondary_col = (0, 153, 255)


def set_colors(primary, secondary):
    global primary_col, secondary_col
    primary_col = primary
    secondary_col = secondary


# Blend between two colors. alpha of 0 -> col1, alpha of 1 -> col2
def blend(col1, col2, alpha):
    weight_col1 = [val * (1-alpha) for val in col1]
    weight_col2 = [val * alpha for val in col2]

    return tuple(int(val[0] + val[1]) for val in zip(weight_col1, weight_col2))


# Alternate sets of leds circle the hat
def alternate(primary, secondary):
    pattern_len = 10
    pattern_high = 5

    sleep_time = 0.1
    cycles = 50

    for t in range(0,cycles):
        for i in range(0, num_pixels):
            val = (i + t) % pattern_len
            pixels[i] = primary if val < pattern_high else secondary
        pixels.show()
        time.sleep(sleep_time)


# Alternate sets of leds circle the hat
def alternate_fast(primary, secondary):
    pattern_len = 14
    pattern_high = 7

    sleep_time = 0.015
    cycles = 150

    for t in range(0,cycles):
        for i in range(0, num_pixels):
            val = (i - t) % pattern_len
            pixels[i] = primary if val < pattern_high else secondary
        pixels.show()
        time.sleep(sleep_time)


def fade_between(primary, secondary):
    cycles = 4
    hold_time = 0.8
    fade_time = 0.2
    fade_cycles = 10

    for cycle in range(0,cycles):
        # Show primary
        pixels.fill(primary)
        pixels.show()
        time.sleep(hold_time)

        # Fade to secondary
        for fade in range(0, fade_cycles):
            alpha = fade / fade_cycles
            pixels.fill(blend(primary, secondary, alpha))
            pixels.show()
            time.sleep(fade_time / fade_cycles)

        # Show secondary
        pixels.fill(secondary)
        pixels.show()
        time.sleep(hold_time)

        # Fade to primary
        for fade in range(0, fade_cycles):
            alpha = fade / fade_cycles
            pixels.fill(blend(secondary, primary, alpha))
            pixels.show()
            time.sleep(fade_time / fade_cycles)


def fill(primary, secondary):
    pixel_time = 0.01
    cycles = 5

    for cycle in range(0, cycles):
        for i in range(0, num_pixels):
            pixels[i] = primary
            pixels.show()
            time.sleep(pixel_time)

        for i in range(0, num_pixels):
            pixels[i] = secondary
            pixels.show()
            time.sleep(pixel_time)


def rand(primary, secondary):
    pixel_time = 0.05
    cycles = 3

    order = [i for i in range(num_pixels)]

    for i in range(0, cycles):
        random.shuffle(order)

        for pix in order:
            pixels[pix] = primary
            pixels.show()
            time.sleep(pixel_time)

        random.shuffle(order)

        for pix in order:
            pixels[pix] = secondary
            pixels.show()
            time.sleep(pixel_time)


def set_pattern(primary, secondary, pattern):
    dimmed_primary = tuple(int(val * dimming) for val in primary)
    dimmed_secondary = tuple(int(val * dimming) for val in secondary)
    pattern(dimmed_primary, dimmed_secondary)


def do_patterns():
    while True:
        set_pattern(primary_col, secondary_col, rand)
        set_pattern(primary_col, secondary_col, fill)
        set_pattern(primary_col, secondary_col, alternate_fast)
        set_pattern(primary_col, secondary_col, fade_between)
        set_pattern(primary_col, secondary_col, alternate)


if __name__ == '__main__':
    thread = threading.Thread(target=do_patterns)
    thread.daemon = True
    thread.start()

    while True:
        try:
            line = sys.stdin.readline()
        except KeyboardInterrupt:
            break

        if not line:
            break

        print('Recieved input: ' + line)

        try:
            tuples = ast.literal_eval(line)
            if len(tuples) != 2:
                raise Exception("Could not find exactly two tuples in input")
            set_colors(tuples[0], tuples[1])
        except Exception as e:
            print('Failed to evaluate input: ' + str(e))
