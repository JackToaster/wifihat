# WIFI HAT

This is the source for a web server on a Raspberry Pi Zero W built into my hat. The web server is built on a python-based framework called Flask.

This was written and built in an evening with no planning whatsoever, so expect an awful mess of spaghetti code.