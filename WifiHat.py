
import pickle

from flask import Flask
from flask import render_template
from flask import request
from flask import Markup

# LED Control process
from pexpect import spawnu as spawn, EOF

led_ctl_proc = spawn("sudo python3 LedControl.py")
led_ctl_proc.delaybeforesend = 0

app = Flask(__name__)

# Setup comments
comments_file = 'comments.pkl'

comments = [] # [{'poster': 'Test', 'text': 'test comment please ignore'}]

try:
    with open(comments_file, 'rb') as file:
        comments = pickle.load(file)
except Exception as e:
    print("Unable to load comments from " + comments_file)


# Main request handler
@app.route('/', methods=['POST', 'GET'])
def control_page():
    if request.method == 'POST':
        print('POST Recieved')
        print(request.form)

        form = request.form
        if 'color1' in form and 'color2' in form:
            try:
                # Get the colors from the string
                color1 = parse_color(form['color1'])
                color2 = parse_color(form['color2'])

                # Write to the led control process
                led_ctl_proc.sendline(repr(color1) + ',' + repr(color2) + '\n')
                led_ctl_proc.expect("Recieved input: ")

            except Exception as e:
                print('Failed to process request: ' + str(e))

        elif 'comment' in form and 'poster' in form:
            print('comment detected')

            # add comment
            comments.insert(0, {'poster': Markup.escape(form['poster']), 'text': Markup.escape(form['comment'])})

            # Write comments to file
            with open(comments_file, 'wb') as cfile:
                pickle.dump(comments, cfile)

    return render_template('template.html', comments=comments)


def parse_color(html_string):
    red_str = html_string[1:3]
    green_str = html_string[3:5]
    blue_str = html_string[5:7]

    return hex_to_int(red_str), hex_to_int(green_str), hex_to_int(blue_str)

def hex_to_int(hex):
    return int("0x" + hex, 0)


if __name__ == '__main__':
    app.run()
